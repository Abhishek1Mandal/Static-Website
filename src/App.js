import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HOME from "./Main_Pages/Home";
import "./CSS/global.css";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/*" element={<HOME />} />
      </Routes>
    </Router>
  );
};

export default App;
